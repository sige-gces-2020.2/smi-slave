Django==3.0.*
django-environ==0.4.*
psycopg2-binary==2.7.*
django-cron==0.5.*
django-polymorphic==2.1.*
djangorestframework==3.11.*
pycodestyle==2.*
retrying==1.3.*
